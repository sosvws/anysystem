# Getting Started

### Reference Documentation
El aplicativo muestra un hola mundo mediante en un war, teniendo un jar de dependencia.
 
 El despliegue tiene 5 fases:
 1. Build -> Se construye en un contenedor de docker
 2. Testing -> Se ejecutan los test del proyecto, generando el reporte de jacoco
 3. Sonar -> Se ejecuta el sonar, llevando los resultados a la instancia del sonar
 4. Artifactory -> Se construye el artefacto en un contenedor de docker y se publica en el artifactory(Jfrog)
 5. Deploy -> Una vez construido el proyecto, se construye la imagen y se envia al docker hub